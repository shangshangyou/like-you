//    求1-100之间所有数的乘积
//    求1-100之间所有奇数的和
//    求1-100之间所有数的平均值
/*
var result = 1;
for (var i=1;i<=100;i++){
    result *= i;
}
console.log(result);

result = 0;
for (var i=1;i<=100;i+=2){
    result += i;
}
console.log(result);

result = 0;
for (var i=1;i<=100;i++){
    result += i;
}
result /= 100;
console.log(result);
*/

/*
* 逐个打印1-100
* 跳过 33 55 72
*
*
* 1
* 2 if(i===2){ccccccc}
* console.log()
* aaaa
* 3
*
* */



// for (var i=1;i<=100;i++){
//     console.log("丫丫丫丫");
//     if (i===33||i===55||i===72){
//         continue;
//     //    从continue的位置开始  跳过本次循环
//     }
//     console.log(i);
// }


// //while循环
// var blood = 1000;
// while (blood>0){
//     blood -= 500;
//     console.log(blood);
// }
// //从100 - 1输出
// var num = 101;
// while (num>1){
//     console.log(--num);
// }
// // 从100 - 1输出
// //遇到50跳出循环 break
// var num = 101;
// while (num>1){
//     --num;
//     console.log(num);
//
//     if (num === 50){
//         break;
//     }
// }

//死循环 每一次输入一个数字  当输入99的时候   跳出循环
// while (1){
//     var num = prompt("请输入一个玩意");
//     if (num === "99"){
//         break;
//     }
// }

//问 是不是喜欢我
//如果回答的  不是 这些（喜欢 喜欢你  我喜欢你） 就一直询问

while (1){
    var result = prompt("是不是喜欢我");
    if (result==="喜欢"||result==="喜欢你"||result==="我喜欢你"){
        document.write("我也喜欢你");
        break;
    }
    //如果点击取消 就继续弹框
    if (result === null){
        result = prompt("是不是喜欢我");
    }
}
